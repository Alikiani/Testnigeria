import React, { Component } from 'react';
import './App.css';


export default class Textshow extends Component {

  // This component doesn't use any properties

  constructor(props) {
    super(props);
    
    this.state = {
      text622825: (<div>show here</div>),
      text622825_plainText: "show here",
    };
  }

  render() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    
    const style_background = {
        width: '100%',
        height: '100%',
     };
    const style_background_outer = {
        backgroundColor: '#f4f4f4',
        pointerEvents: 'none',
     };
    const style_text = {
        fontSize: 15.1,
        fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", sans-serif',
        color: 'rgba(0, 0, 0, 0.8500)',
        textAlign: 'center',
     };
    const style_text_outer = {
        pointerEvents: 'none',
     };
    
    return (
      <div className="Textshow" style={baseStyle}>
        <div className="background">
          <div className='appBg containerMinHeight elBackground819856' style={style_background_outer}>
            <div style={style_background} />
          
          </div>
          
        </div>
        <div className="layoutFlow">
          <div className='elText622825' style={style_text_outer}>
            <div style={style_text}>
              <div>{this.props.text}</div>
            </div>
          
          </div>
          
        </div>
      </div>
    )
  }
  

}
