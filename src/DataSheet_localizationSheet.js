import DataSheetBase from './DataSheetBase.js';

export default class DataSheet_localizationSheet extends DataSheetBase {

  constructor(id) {
    super(id);
    this.requestedKeyPath = "";  // this value can be specified in the React Studio data sheet UI
  }

  makeDefaultItems() {
    // eslint-disable-next-line no-unused-vars
    let key = 1;
    // eslint-disable-next-line no-unused-vars
    let item;
    
    item = {};
    item.key = key++;
    this.items.push(item);
    item['key'] = "start_field_747892";
    item['en'] = "Enter your text here";
    
    item = {};
    item.key = key++;
    this.items.push(item);
    item['key'] = "start_button_145092";
    item['en'] = "Show it";
    
    item = {};
    item.key = key++;
    this.items.push(item);
    item['key'] = "start_text_947609";
    item['en'] = "New text. Double-click to edit";
    
    item = {};
    item.key = key++;
    this.items.push(item);
    item['key'] = "textshow_text_367994";
    item['en'] = "New text. Double-click to edit";
    
    item = {};
    item.key = key++;
    this.items.push(item);
    item['key'] = "Textfetch";
    item['en'] = "";
  }

  getStringsByLanguage = () => {
    let stringsByLang = {};
    for (let row of this.items) {
      const locKey = row.key;
      for (let key in row) {
        if (key === 'key')
          continue;
        let langObj = stringsByLang[key] || {};
        langObj[locKey] = row[key];
        stringsByLang[key] = langObj;
      }
    }
    return stringsByLang;
  }

}
