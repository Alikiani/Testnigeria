import React, { Component } from 'react';
import './App.css';
import Textshow from './Textshow';

// UI framework component imports
import Button from 'muicss/lib/react/button';


export default class StartScreen extends Component {

  // Properties used by this component:
  // appActions, deviceInfo

  constructor(props) {
    super(props);
    this.state = {
      nameinput: '',
      clicked: false
    };
    this.show = this.show.bind(this);
  }

  show(e) {
    this.setState({
      clicked: true
    })
  }

  textInputChanged_nameinput = ({ target: { value } }) => {
    this.setState({ nameinput: value, clicked: false });
  }
  
  render() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};

    const { clicked, nameinput } = this.state;
    if (this.props.transitionId && this.props.transitionId.length > 0 && this.props.atTopOfScreenStack && this.props.transitionForward) {
      baseStyle.animation = '0.25s ease-in-out '+this.props.transitionId;
    }
    if ( !this.props.atTopOfScreenStack) {
      layoutFlowStyle.height = '100vh';
      layoutFlowStyle.overflow = 'hidden';
    }
    
    const style_background = {
        width: '100%',
        height: '100%',
     };
    const style_background_outer = {
        backgroundColor: '#f4f4f4',
        pointerEvents: 'none',
     };
    const style_nameinput = {
        display: 'block',
        backgroundColor: 'white',
        paddingLeft: '1rem',
        boxSizing: 'border-box', // ensures padding won't expand element's outer size
     };
    const style_button = {
        display: 'block',
        color: 'white',
        textAlign: 'center',
        cursor: "pointer"
     };
    const style_button_outer = {
     };
    
    return (
      <div className="AppScreen StartScreen" style={baseStyle}>
        <div className="background">
          <div className='appBg containerMinHeight elBackground110902' style={style_background_outer}>
            <div style={style_background} />
          
          </div>
          
        </div>
        <div className="layoutFlow" style={layoutFlowStyle}>
          <div className='baseFont elNameinput747892'>
             <input style={style_nameinput} type="password" placeholder={this.props.locStrings.start_field_747892} onChange={this.textInputChanged_nameinput} defaultValue={this.state.nameinput}  />
          </div>
          
          <div className='actionFont elButton145092' style={style_button_outer}>
            <Button
              color="accent"
              style={style_button}
              onClick={this.show}
            >
              {this.props.locStrings.start_button_145092}
            </Button>
          
          </div>
          
          <div className='hasNestedComps elTextshow463992'>
            <div>
              {clicked &&
                <Textshow
                  text={nameinput}
                  appActions={this.props.appActions}
                  deviceInfo={this.props.deviceInfo}
                  locStrings={this.props.locStrings}
                />              
              }
            </div>
          
          </div>
          
        </div>
      </div>
    )
  }
  

}
